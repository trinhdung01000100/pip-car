import { ButtonProps, Form, Input, InputNumber, Space } from 'antd';
import { useRouter } from 'next/router';
import { Fragment, useState } from 'react';
import { BsFillPersonPlusFill } from 'react-icons/bs';
import useApp from 'src/hooks/useApp';
import { useCreateDriverMutation } from 'src/redux/query/driver.query';
import { ErrorCode } from 'src/types/response.types';
import { mappedErrorToFormError } from 'src/utils/utils-error';
import Button from '../button/Button';
import Modal from './Modal';

function CreateDriverModal({ buttonProps }: { buttonProps?: Omit<ButtonProps, 'onClick'> }) {
  const {
    query: { agencyId },
  } = useRouter();
  const { notification } = useApp();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [form] = Form.useForm();

  const [createMutate, { isLoading }] = useCreateDriverMutation();

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    form
      .validateFields()
      .then((formData) => {
        createMutate({
          agency: String(agencyId),
          ...formData,
          lat: formData.lat_address,
          long: formData.long_address,
        })
          .unwrap()
          .then(({ data, message }) => {
            notification.success({ message, placement: 'bottomLeft' });
            form.resetFields();
            setIsModalOpen(false);
          })
          .catch((err) => {
            if ([ErrorCode.BadRequest, ErrorCode.DataNotFound].includes(err.response_code))
              notification.error({ message: err.error[0].message, placement: 'bottomLeft' });
            if ([ErrorCode.RequestValidationError].includes(err.response_code))
              form.setFields(mappedErrorToFormError(err.error));
          });
      })
      .catch((info) => {
        console.log('Validate Failed:', info);
      });
  };

  const handleCancel = () => {
    form.resetFields();
    setIsModalOpen(false);
  };

  return (
    <Fragment>
      <Button
        loading={isModalOpen}
        block
        icon={<BsFillPersonPlusFill />}
        type='dashed'
        size='large'
        onClick={showModal}
        {...buttonProps}
      >
        Create new driver
      </Button>
      {isModalOpen && (
        <Modal
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          title='Create Driver'
          okText='Create'
          cancelText='Cancel'
          okButtonProps={{ size: 'large', disabled: isLoading }}
          cancelButtonProps={{ size: 'large', disabled: isLoading }}
          style={{ top: 24 }}
        >
          <Form
            form={form}
            layout='vertical'
            size='large'
            initialValues={{
              address: 'Nhà B1, Đại học Bách Khoa Hà Nội',
              lat_address: '21.00383',
              long_address: '105.83916',
              lat: '21.00383',
              long: '105.83916',
            }}
            disabled={isLoading}
          >
            <Form.Item
              name='name'
              label='Name'
              hasFeedback={isLoading}
              validateStatus={isLoading ? 'validating' : undefined}
              rules={[{ required: true, message: '• Name is required' }]}
            >
              <Input placeholder='Driver name..., ex: Pipcar Driver' />
            </Form.Item>
            <Form.Item
              name='phone'
              label='Phone'
              hasFeedback={isLoading}
              validateStatus={isLoading ? 'validating' : undefined}
              rules={[{ required: true, message: '• Phone is required' }]}
            >
              <Input type='tel' placeholder='Phone number..., ex: 0989123456' />
            </Form.Item>
            <Form.Item
              name='license_id'
              label='LicenseID'
              hasFeedback={isLoading}
              validateStatus={isLoading ? 'validating' : undefined}
              rules={[{ required: true, message: '• LicenseID is required' }]}
            >
              <Input type='tel' placeholder='LicenseID..., ex: 123400006789' />
            </Form.Item>
            <Form.Item
              name='address'
              label='Address'
              rules={[{ required: true, message: '• Address is required' }]}
            >
              <Input.TextArea placeholder='Address...' autoSize={{ minRows: 2 }} showCount />
            </Form.Item>
            <Form.Item label='Map' required tooltip='Lat | Long'>
              <Space.Compact block size='large'>
                <Form.Item
                  name='lat_address'
                  noStyle
                  rules={[{ required: true, message: '• lat_address is required' }]}
                >
                  <InputNumber<string>
                    placeholder='Lat...'
                    stringMode
                    step='0.00001'
                    style={{ width: '100%' }}
                  />
                </Form.Item>
                <Form.Item
                  name='long_address'
                  noStyle
                  rules={[{ required: true, message: '• long_address is required' }]}
                >
                  <InputNumber<string>
                    placeholder='Long...'
                    stringMode
                    step='0.00001'
                    style={{ width: '100%' }}
                  />
                </Form.Item>
              </Space.Compact>
            </Form.Item>
            {/* {!!latAddressForm && !!longAddressForm && (
                <iframe loading='lazy' src={iframeMapSrc}></iframe>
              )} */}
            <iframe
              height='100'
              src='https://www.google.com/maps/embed?language=en&pb=!1m14!1m8!1m3!1d931.1760760275844!2d105.8469656!3d21.004487!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76827aaaab%3A0xf0580eb2ff0e1b64!2zVHLGsOG7nW5nIEPDtG5nIE5naOG7hyBUaMO0bmcgVGluIFRydXnhu4FuIFRow7RuZyAtIMSQ4bqhaSBI4buNYyBCw6FjaCBraG9hIEjDoCBu4buZaQ!5e0!3m2!1svi!2s!4v1673413674924!5m2!1svi!2s'
              loading='lazy'
              referrerPolicy='no-referrer-when-downgrade'
            ></iframe>
          </Form>
        </Modal>
      )}
    </Fragment>
  );
}

export default CreateDriverModal;
