import { createApi } from '@reduxjs/toolkit/query/react';
import { TCar } from 'src/types/car.types';
import { TDriver } from 'src/types/driver.types';
import { ID, TBaseFilter } from 'src/types/global.types';
import { TResponse, TResponseWithMeta } from 'src/types/response.types';
import baseQueryWithReauth from '../app/baseQueryWithReauth';

export type TListFilter = TBaseFilter & {
  agency?: string;
  isDriven?: boolean;
};

export type TCreateCarData = {
  agency_id: ID;
  plates: string;
  type: string;
  name: string;
  lat?: string;
  long?: string;
  updated_gps_time?: number;
};
export type TUpdateCarDetailData = {};

export const carApi = createApi({
  reducerPath: 'carApi',
  baseQuery: baseQueryWithReauth,
  tagTypes: ['Cars'],
  keepUnusedDataFor: 600,
  endpoints: (builder) => ({
    // [ADMIN,PM]
    getCarList: builder.query<TResponseWithMeta<{ car_list: TCar[] }>, TListFilter>({
      query: (filter) => ({ url: '/car', method: 'get', params: filter }),
      providesTags: (result) =>
        !!result?.data?.car_list.length
          ? [
              ...result.data.car_list.map(({ _id }) => ({
                type: 'Cars' as const,
                id: _id,
              })),
              { type: 'Cars', id: 'LIST' },
            ]
          : [{ type: 'Cars', id: 'LIST' }],
    }),
    // [ADMIN,PM]
    // getCarListInfinite: builder.query<TResponseWithMeta<{ car_list: TCar[] }>, TListFilter>({
    //   query: (filter) => ({ url: '/car', method: 'get', params: filter }),
    //   providesTags: (result) =>
    //     !!result?.data?.car_list.length
    //       ? [
    //           ...result.data.car_list.map(({ _id }) => ({
    //             type: 'Cars' as const,
    //             id: _id,
    //           })),
    //           { type: 'Cars', id: 'LIST' },
    //         ]
    //       : [{ type: 'Cars', id: 'LIST' }],
    //   serializeQueryArgs: ({ endpointName }) => {
    //     return endpointName;
    //   },
    //   // Always merge incoming data to the cache entry
    //   merge: (currentCache, newItems) => {
    //     (currentCache?.data?.car_list || []).push(...(newItems?.data?.car_list || []));
    //     uniqBy(currentCache.data?.car_list, '_id');
    //   },
    //   // Refetch when the page arg changes
    //   forceRefetch({ currentArg, previousArg }) {
    //     return JSON.stringify(currentArg) !== JSON.stringify(previousArg);
    //   },
    // }),
    // [ADMIN,PM]
    getCarDetail: builder.query<
      TResponse<{ car_detail: TCar; driver_detail: TDriver | null }>,
      string
    >({
      query: (carId) => ({ url: `/car/detail/${carId}`, method: 'get' }),
      providesTags: (result, error, id) => [{ type: 'Cars', id }],
    }),
    // [ADMIN,PM]
    createCar: builder.mutation<TResponse, TCreateCarData>({
      query: ({ agency_id, ...data }) => ({
        url: `/car?agency=${agency_id}`,
        method: 'post',
        data: data,
      }),
      invalidatesTags: [{ type: 'Cars', id: 'LIST' }],
    }),
    // [ADMIN,PM]
    deleteCar: builder.mutation<TResponse, string>({
      query: (carId) => ({ url: `/car/${carId}`, method: 'delete' }),
      invalidatesTags: [{ type: 'Cars', id: 'LIST' }],
    }),
  }),
});
export const {
  useCreateCarMutation,
  useGetCarDetailQuery,
  useGetCarListQuery,
  useDeleteCarMutation,
} = carApi;
